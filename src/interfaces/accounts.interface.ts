import { Document } from 'mongoose';

export interface Accounts extends Document {
  readonly guid: string;
  readonly name: string;
  readonly category: string;
  readonly tag: string;
  readonly balance: number;
  readonly availableBalance: number;
}