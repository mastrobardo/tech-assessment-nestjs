import { Document } from 'mongoose';

export interface Transactions extends Document {
  readonly owner: string;
  readonly orderCode: string;
  readonly orderId: number;
  readonly credit: number;
  readonly balance: number;
}