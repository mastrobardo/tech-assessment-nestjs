//Account Name, Category, Tag, Balance and Available balance
export class CreateAccountDto {
  name: string;
  category: string;
  tag: string;
  balance: number;
  availableBalance: number;
}