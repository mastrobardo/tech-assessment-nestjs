import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AccountsController } from './accounts/accounts.controller';
import { TransactionsController } from './transactions/transactions.controller';
import { AccountsModule } from './accounts/accounts.module';
import { TransactionsModule } from './transactions/transactions.module'
import { AppGateway } from './app.gateway';

@Module({
  imports: [AccountsModule, TransactionsModule],
  controllers: [AppController, AccountsController, TransactionsController],
  providers: [AppService, AppGateway],
})
export class AppModule {}
