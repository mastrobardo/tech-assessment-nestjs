import * as mongoose from 'mongoose';

export const TransactionsSchema = new mongoose.Schema({
  owner: String,
  orderCode: String,
  orderId: Number,
  credit: Number,
  balance: Number
});