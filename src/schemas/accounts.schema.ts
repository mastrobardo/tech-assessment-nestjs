import * as mongoose from 'mongoose';

export const AccountsSchema = new mongoose.Schema({
  guid: String,
  name: String,
  category: String,
  tag: String,
  balance: Number,
  availableBalance: Number,
});