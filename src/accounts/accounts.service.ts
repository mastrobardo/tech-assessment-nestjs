import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Accounts } from '../interfaces/accounts.interface';

@Injectable()
export class AccountsService {
  constructor(
    @Inject('ACCOUNTS_MODEL')
    private readonly accountModel: Model<Accounts>,
  ) {}

  async findAll(): Promise<Accounts[]> {
    return this.accountModel.find().exec();
  }

  async findOne(guid: string): Promise<Accounts> {
    return this.accountModel.findOne({guid: guid}).exec();
  }
}