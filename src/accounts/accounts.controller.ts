
import { Controller, Get, Header, Param } from '@nestjs/common';
import { AccountsService } from './accounts.service'
import { Accounts } from 'src/interfaces/accounts.interface';

@Controller('accounts')
export class AccountsController {
  constructor(private readonly accountsService: AccountsService){}
  @Get()
  @Header('Cache-Control', 'none')
  async findAll(): Promise<Accounts[]> {
    const accounts = await this.accountsService.findAll();
    return accounts;
  }

  @Get(':guid')
  @Header('Cache-Control', 'none')
  async getAccount(@Param('guid') guid: string): Promise<Accounts> {
    const account = await this.accountsService.findOne(guid);
    return account;
  }
}