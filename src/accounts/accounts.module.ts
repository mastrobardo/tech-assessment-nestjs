import { Module } from '@nestjs/common';
import {AccountsController } from './accounts.controller';
import {AccountsService } from './accounts.service';
import {AccountsProviders } from './accounts.providers';
import { DatabaseModule } from '../db/db.module';

@Module({
  imports: [DatabaseModule],
  exports: [AccountsService],
  controllers: [AccountsController],
  providers: [
   AccountsService,
    ...AccountsProviders,
  ],
})
export class AccountsModule {}