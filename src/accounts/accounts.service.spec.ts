import { Test } from '@nestjs/testing';
import { AccountsService } from './accounts.service';
import { AccountsProviders } from './accounts.providers';
import { databaseProvider } from '../db/db.provider';

describe('ItemService', () => {
  let service: AccountsService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [...AccountsProviders, ...databaseProvider, AccountsService],
    }).compile();

    service = module.get<AccountsService>(AccountsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});