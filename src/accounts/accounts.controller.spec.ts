import { Test, TestingModule } from "@nestjs/testing";
import { AccountsController } from './accounts.controller'
import { AccountsModule } from "./accounts.module";

describe("Accounts Controller", () => {
 let module: TestingModule;
 let accountsController: AccountsController;

 beforeAll(async () => {
  module = await Test.createTestingModule({
  imports: [AccountsModule],
  controllers: [
      AccountsController
  ]
   }).compile()
   
   accountsController = module.get(AccountsController);
 });
  afterEach(() => {
      jest.resetAllMocks();
  })
  it('should be defined', () => {
    expect(accountsController).toBeDefined();
  })
  
})