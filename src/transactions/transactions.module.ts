import { Module } from '@nestjs/common';
import {TransactionsController } from './transactions.controller';
import {TransactionsService } from './transactions.service';
import { TransactionsProviders } from './transactions.providers';
import { DatabaseModule } from '../db/db.module';

@Module({
  imports: [DatabaseModule],
  exports: [TransactionsService],
  controllers: [TransactionsController],
  providers: [
   TransactionsService,
    ...TransactionsProviders,
  ],
})
export class TransactionsModule {}