
import { Controller, Get, Header, Param } from '@nestjs/common';
import { TransactionsService } from './transactions.service'
import { Transactions } from 'src/interfaces/transactions.interface';

@Controller('transactions')
export class TransactionsController {
  constructor(private readonly transactionsService: TransactionsService){}
  @Get()
  @Header('Cache-Control', 'none')
  async findAll(): Promise<Transactions[]> {
    const transactions = await this.transactionsService.findAll();
    return transactions;
  }

  @Get(':owner')
  @Header('Cache-Control', 'none')
  async getAccount(@Param('owner') owner: string): Promise<Transactions[]> {
    const transactions = await this.transactionsService.find(owner);
    return transactions;
  }
}