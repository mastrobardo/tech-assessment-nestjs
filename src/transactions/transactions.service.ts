import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Transactions } from '../interfaces/transactions.interface';

@Injectable()
export class TransactionsService {
  constructor(
    @Inject('TRANSACTIONS_MODEL')
    private readonly transactionsModel: Model<Transactions>,
  ) {}

  async findAll(): Promise<Transactions[]> {
    return this.transactionsModel.find().exec();
  }

  async find(owner: string): Promise<Transactions[]> {
    return this.transactionsModel.find({owner: owner}).exec();
  }
}