import { Connection } from 'mongoose';
import { TransactionsSchema } from '../schemas/transactions.schema';

export const TransactionsProviders = [
  {
    provide: 'TRANSACTIONS_MODEL',
    useFactory: (connection: Connection) => connection.model('Transactions', TransactionsSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];