THE SERVER    

disclaimer: never used nestjs, and i had still to work this week end ( some error in production, and today we have a demo with the client: could'nt say no. )
So, the assessment is kinda rushed, the test are almost not existent, and the commits as well are a little messy: i worked on this assessment during the breaks. I apologise, but obviously my current work comes first: i'm a professional.    

HOW TO RUN    

- run mongo (this step might vary depending on the machines: either use the ./runMongo.sh script, or your own was). Please note the code is assuming mongo running at the default port (ie: mongodb://127.0.0.1:27017 ). Change db.provider setup to use different ports     
- run the seeders: ./seedAccounts.sh and ./seedTransactions.sh. They are just running the good old mongo-import    
- run the server, in the usual nest way   

Please Note:

Transactions are available only for the first 2 users ( so to test the master/detail implementation )